package com.thinknsync.bluetoothposprinter;
import android.bluetooth.BluetoothDevice;

import java.io.UnsupportedEncodingException;
import java.util.Set;


public interface BlueToothOperations {
    boolean checkBluetoothCompatibility();
    boolean isBluetoothEnabled();
    BluetoothDeviceModel getBluetoothDeviceModel();
    void setBluetoothDeviceModel(BlueToothPosDevice btDeviceModel);
    void turnBluetoothOn();
    Set<BluetoothDevice> scanForBluetoothDevices();
    boolean checkForPairedDevice() throws UnsupportedEncodingException;
    void pairDevice();
    void conenctPrinter();
    void printData(String printMsg);
    boolean writeWithFormat(byte[] buffer, final byte[] pFormat, final byte[] pAlignment);
}
